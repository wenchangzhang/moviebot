import os
import re
from slackclient import SlackClient
import requests
import json
import time
import logging
from requests.exceptions import ConnectionError

# constants
MENTION_REGEX = "^<@(|[WU].+?)>(.*)"
DM_REGEX = "^D"
TITLE_REGEX = "^tt[0-9]+$"
NAME_REGEX = "^nm[0-9]+$"
MOVIEDB_REGEX = "^[0-9]+$"

valid_commands = (
    ["help", "bacon", "title", "name", "trailer", "trending"]
)
rest_url = "http://localhost:9999"

#
# Environment variables
#
# MOVIEBOT_OAUTH_TOKEN - This is the Slack token
# MOVIEBOT_MOVIEDB_TOKEN - This is the API token for themoviedb.org
#

logging.basicConfig()
slack_client = SlackClient(os.environ['MOVIEBOT_OAUTH_TOKEN'])
rest_reply_timeout_secs = 30
exception_silence_interval_secs = 120
max_exceptions_to_report_per_interval = 3
user_last_command_history = {}
max_titles_to_display = 15
trending_cap = 3

class MovieDB(object):
    def __init__ (self):
        self.moviedb_token = os.environ['MOVIEBOT_MOVIEDB_TOKEN']

        #
        # Re-use the same session for each request
        #
        self.moviedb_session = requests.Session()
        self.moviedb_url = "https://api.themoviedb.org/3"
        self.moviedb_timeout = 5
        self.cached_ids = {}

    def get_trending(
        self,
    ):
        request_url = "{}/trending/all/day".format(
            self.moviedb_url,
        )
        params = {
            'api_key': self.moviedb_token,
        }
        print "Sending trending request {}".format(request_url)
        r = self.moviedb_session.get(
            request_url,
            params=params,
            timeout=self.moviedb_timeout
        )

        if r.status_code != requests.codes.ok:
            print (
                "Unexpected response from MovieDB server:\n{}".format(
                    r
                )
            )
            r.raise_for_status()

        return r

    def get_name_detail(
        self,
        moviedb_id,
    ):
        request_url = "{}/person/{}".format(
            self.moviedb_url,
            moviedb_id
        )
        params = {
            'api_key': self.moviedb_token,
        }
        print "Sending person request {}".format(request_url)

        r = self.moviedb_session.get(
            request_url,
            params=params,
            timeout=self.moviedb_timeout
        )

        if r.status_code != requests.codes.ok:
            print (
                "Unexpected response from MovieDB server:\n{}".format(
                    r
                )
            )
            r.raise_for_status()

        return r

    def get_title_by_imdb(
        self,
        imdb_id,
        get_trailer=False
    ):
        request_url = "{}/find/{}".format(
            self.moviedb_url,
            imdb_id
        )
        params = {
            'api_key': self.moviedb_token,
            'external_source': 'imdb_id',
            'include_adult': False,
        }

        print "Sending moviedb request {}".format(request_url)

        r = self.moviedb_session.get(
            request_url,
            params=params,
            timeout=self.moviedb_timeout
        )

        if r.status_code != requests.codes.ok:
            print (
                "Unexpected response from MovieDB server:\n{}".format(
                    r
                )
            )
            r.raise_for_status()

        return r.json()

    def invalidate_cache(
        self
    ):
        self.cached_ids = {}

    def get_cached(
        self,
        id,
    ):
        print "searching cache for {}".format(id)
        if id in self.cached_ids:
            print "cache hit for {}".format(id)
            print self.cached_ids[id]

            return self.cached_ids[id]
        print self.cached_ids
        return None

    def get_detail(
        self,
        id,
        media_type
    ):
        request_url = "{}/{}/{}".format(
            self.moviedb_url,
            media_type,
            id,
        )
        params = {
            'api_key': self.moviedb_token,
            'append_to_response': 'videos',
            'include_adult': False,
        }

        r = self.moviedb_session.get(
            request_url,
            params=params,
            timeout=self.moviedb_timeout
        )

        if r.status_code != requests.codes.ok:
            print (
                "Unexpected response from MovieDB server:\n{}".format(
                    r
                )
            )
            r.raise_for_status()

        if r.json():
            print "Adding to cache"
            print r.json()
            self.cached_ids[r.json()['id']] = r.json()

        return r.json()

    def search_title(
        self,
        query_string
    ):
        request_url = "{}/search/multi/".format(
            self.moviedb_url,
        )
        params = {
            'api_key': self.moviedb_token,
            'query': query_string,
            'include_adult': False,
        }

        r = self.moviedb_session.get(
            request_url,
            params=params,
            timeout=self.moviedb_timeout
        )

        if r.status_code != requests.codes.ok:
            print (
                "Unexpected response from MovieDB server:\n{}".format(
                    r
                )
            )
            r.raise_for_status()

        if r.json()['total_results'] > 0:
            print "Adding to cache"
            for result in r.json()['results']:
                print result
                self.cached_ids[result['id']] = result

        return r

#
# Initialize our themoviedb.org object
#
moviedb_client = MovieDB()

def parse_direct_mention(message_text):
    """
        Finds a direct mention (a mention that is at the beginning) in message text
        and returns the user ID which was mentioned. If there is no direct mention, returns None
    """
    matches = re.search(MENTION_REGEX, message_text)
    #starterbot id is group 1
    # the first group contains the username, the second group contains the remaining message
    return (matches.group(1), matches.group(2).strip()) if matches else (None, None)


def parse_bot_commands(event):
    """
        Parses a list of events coming from the Slack RTM API to find bot commands.
        If a bot command is found, this function returns a tuple of command and channel.
        If its not found, then this function returns None, None.
    """
    if event["type"] == "message" and not "subtype" in event:

        #
        # Direct message
        #
        # {u'source_team': u'T024H4CK1', u'text': u'test', u'ts': u'1525551377.000019', u'user': u'U1GF1MGBZ', u'team': u'T024H4CK1', u'type': u'message', u'channel': u'DAJDE21MX'}
        # If a channel starts with C, it's public.
        # If a channel starts with D, it's a DM.
        # If a channel starts with G, it's either a private channel or multi-person DM
        #
        # The above is subject to change
        #
        user_id = None
        message = None
        dm_matches = re.search(DM_REGEX, event["channel"])
        if dm_matches:
            user_id = event["user"]
            message = event["text"]
        else:
            user_id, message = parse_direct_mention(event["text"])

        if message is None:
            return None, None, None, None

        #
        # The message came from this bot.
        #
        if dm_matches and user_id == starterbot_id:
            return None, None, None, None

        #
        # Somebody else was @mentioned in this message.
        # Ignore since it's not directed at the bot.
        #
        if not dm_matches and user_id and user_id != starterbot_id:
            return None, None, None, None

        command = message.lower().split(" ")[0]

        if (
            message is not None and
            command in valid_commands
        ):
            return message, event["channel"], event["user"], event["ts"]
        elif message and message[0] == "!":
            if event["user"] not in user_last_command_history:
                return None, None, None, None

            #
            # Rewrite the message
            #
            message = "{} {}".format(
                user_last_command_history[event["user"]],
                message[1:].lstrip()
            ).rstrip()
            return message, event["channel"], event["user"], event["ts"]

    return None, None, None, None


def disambiguate_names(json_response):
    num_results = 0
    bacon_msg = u""

    for x in json_response:
        num_results += 1
        name_msg = u"`{}` {}".format(
            x['nameID'],
            x['name']
        )

        if 'birthYear' in x:
            name_msg += u" ({})".format(
                x['birthYear']
            )

        if 'titles' in x:
            titles_array = []

            #
            # Cap the titles at 4
            #
            for c in x['titles'][:4]:
                titles_array.append(c['title'])

            if titles_array:
                titles_string = u", ".join(titles_array)
            else:
                titles_string = u"No titles information"
            name_msg += u" ({})".format(titles_string)

        bacon_msg += u"{}\n".format(name_msg)

    return num_results, bacon_msg


def disambiguate_titles(json_response):
    num_results = 0
    bacon_msg = u""

    for x in json_response:
        num_results += 1
        title_msg = u"`{}` {}".format(
            x['titleID'],
            x['title']
        )

        if 'startYear' in x:
            title_msg += u" ({})".format(
                x['startYear']
            )

        if 'cast' in x:
            cast_array = []

            #
            # Cap the cast at 4
            #
            for c in x['cast'][:4]:
                cast_array.append(c['name'])

            if cast_array:
                cast_string = u", ".join(cast_array)
            else:
                cast_string = u"No cast information"
            title_msg += u" ({})".format(cast_string)

        bacon_msg += u"{}\n".format(title_msg)

    return num_results, bacon_msg


def bacon_name(name_id):
    bacon_url = "{}/bacon/name/{}".format(
        rest_url,
        name_id
    )
#    payload= (
#        {
#            'show_cast': "true",
#            'show_roles': "true"
#        }
#    )
    print "Sending request {}".format(bacon_url)
    r = bacon_session.get(bacon_url, timeout=rest_reply_timeout_secs)

    if r.status_code != requests.codes.ok:
        print (
            "Unexpected response from REST server:\n{}".format(
                r
            )
        )
        r.raise_for_status()
    bacon_msg = u""

    return r


def bacon_title(title_id):
    bacon_url = "{}/bacon/title/{}".format(
        rest_url,
        title_id
    )

    print "Sending request {}".format(bacon_url)
    r = bacon_session.get(bacon_url, timeout=rest_reply_timeout_secs)

    if r.status_code != requests.codes.ok:
        print (
            "Unexpected response from REST server:\n{}".format(
                r
            )
        )
        r.raise_for_status()
    bacon_msg = u""

    return r


def name_get(name_id):
    bacon_url = "{}/name/{}".format(rest_url, name_id)
    payload= (
        {
            'show_roles': "true",
            'show_titles': "true",
        }
    )
    print "Sending request {}".format(bacon_url)
    r = bacon_session.get(bacon_url, params=payload, timeout=rest_reply_timeout_secs)

    if r.status_code == 404:
        return None
    if r.status_code != requests.codes.ok:
        print (
            "Unexpected response from REST server:\n{}".format(
                r
            )
        )
        r.raise_for_status()

    return r.json()


def title_get(title_id):
    bacon_url = "{}/title/{}".format(rest_url, title_id)
    payload= (
        {
            'show_cast': "true",
            'show_roles': "true",
            'show_non_principals': "true"
        }
    )
    print "Sending request {}".format(bacon_url)
    r = bacon_session.get(bacon_url, params=payload, timeout=rest_reply_timeout_secs)

    if r.status_code == 404:
        return None
    if r.status_code != requests.codes.ok:
        print (
            "Unexpected response from REST server:\n{}".format(
                r
            )
        )
        r.raise_for_status()

    return r.json()


def name_search(search_string):
    bacon_url = "{}/search/name".format(rest_url)
    payload= (
        {
            'q' : search_string,
            'show_titles': "true",
        }
    )
    print "Sending request {}".format(bacon_url)
    r = bacon_session.get(bacon_url, params=payload, timeout=rest_reply_timeout_secs)

    if r.status_code != requests.codes.ok:
        print (
            "Unexpected response from REST server:\n{}".format(
                r
            )
        )
        r.raise_for_status()
    bacon_msg = u""

    return r


def title_search(
    search_string,
):
    bacon_url = "{}/search/title".format(rest_url)
    payload= (
        {
            'q' : search_string,
            'show_cast': "true",
            'show_roles': "true"
        }
    )
    print "Sending request {}".format(bacon_url)
    r = bacon_session.get(bacon_url, params=payload, timeout=rest_reply_timeout_secs)

    if r.status_code != requests.codes.ok:
        print (
            "Unexpected response from REST server:\n{}".format(
                r
            )
        )
        r.raise_for_status()

    #
    # If we got no results, try falling back to searching themoviedb.org
    #
    if len(r.json()) == 0:
        print "Falling back to themoviedb.org"
        fallback_r = moviedb_client.search_title(search_string)
        if fallback_r.status_code != requests.codes.ok:
            fallback_r.raise_for_status()

        #
        # Package results into the common format
        #
        if fallback_r.json()['total_results'] == 0:
            return r.json()

        fallback_results = []
        for result in fallback_r.json()['results']:
            title_str = u""
            if 'name' in result:
                title_str = result['name']
            elif 'title' in result:
                title_str = result['title']

            if len(title_str) > 0:
                fallback_results.append({
                    'titleID': result['id'],
                    'title': u"{}".format(title_str)
                })
        if len(fallback_results) > 0:
            return fallback_results

    return r.json()


def parse_name(name_id, channel):
    r = name_get(name_id)

    if not r:
        slack_client.api_call(
            "chat.postMessage",
            channel=channel,
            text="Unknown name ID {}".format(name_id)
        )
        return

    json_response = r

    name_msg = u"`{}` *{}*".format(
        json_response['nameID'],
        json_response['name']
    )

    if 'birthYear' in json_response:
        if (
            'deathYear' in json_response and
            json_response['deathYear'] != json_response['birthYear']
        ):
            name_msg += u" ({}-{})".format(
                json_response['birthYear'],
                json_response['deathYear'],
            )
        else:
            name_msg += u" ({})".format(
                json_response['birthYear'],
            )

    attachments = []

    #
    # Get extra title info from themoviedb
    #
    moviedb_info_json = moviedb_client.get_title_by_imdb(name_id)
    msg_text = ""
    thumbnail = {}
    actor_id = None

    if moviedb_info_json:
        for result in ['person_results']:
            for title in moviedb_info_json[result]:
                if 'profile_path' in title:
                    #    "poster_sizes": [
                    #      "w92",
                    #      "w154",
                    #      "w185",
                    #      "w342",
                    #      "w500",
                    #      "w780",
                    #      "original"
                    #    ],
                    #            videos
                    #            tv_episode_results
                    #            tv_season_results
                    #            tv_results
                    #            person_results
                    #            movie_results
                    #
                    thumbnail = "{}{}".format(
                        "https://image.tmdb.org/t/p/w342",
                        title['profile_path']
                    )
                    actor_id = title['id']

                    r = moviedb_client.get_name_detail(
                        actor_id
                    )
                    if r:
                        msg_text += r.json()['biography']

    title_msg = u"No title information"
    capped = False
    if ('titles' in json_response and len(json_response['titles']) > 0):
        title_msg = u""
        capped = len(json_response['titles']) > max_titles_to_display

        #
        # Cap the number of titles displayed to be nice.
        # Randomize the titles lsited.
        #
        title_array = json_response['titles']
#        title_array = title_array[-max_titles_to_display:]

        for x in title_array:
            title_msg += u"`{}` {}".format(
                x['titleID'],
                x['title']
            )
            if 'startYear' in x:
                title_msg += u" ({})".format(
                    x['startYear']
                )
            if 'role' in x:
                title_msg += u" _{}_".format(
                    u", ".join(x['role'])
                )
            title_msg += u"\n"

    summary_msg = msg_text + "\n" + title_msg
    attachments = [{
        'pretext': name_msg,
        'thumb_url': thumbnail,
        'text': summary_msg,
        'mrkdwn_in': ["text", "pretext"],
    }]

    slack_client.api_call(
        "chat.postMessage",
        channel=channel,
        attachments=attachments,
    )

    return

def handle_trending(
    channel,
):
    r = moviedb_client.get_trending()
    if r:
        blocks = []
        for result in r.json()['results'][:trending_cap]:
            for type in ['name', 'title']:
                if type in result:
                    thumbnail = None

                    for path in ['poster_path', 'profile_path']:
                        if path in result:
                            thumbnail = "{}{}".format(
                                "https://image.tmdb.org/t/p/w342",
                                result[path]
                            )

                    #
                    # Add name/title
                    #
                    blocks.append({
                        'type': 'section',
                        'text': {
                            'text': u"*{}*".format(result[type]),
                            'type': 'mrkdwn',
                        },
                    })

                    overview = None
                    for bio_overview in ['overview','biography']:
                        if bio_overview in result:
                            blocks.append({
                                'type': 'section',
                                'text': {
                                    'text': result[bio_overview],
                                    'type': 'mrkdwn',
                                },
                                'accessory': {
                                    'image_url': thumbnail,
                                    'type': 'image',
                                    'alt_text': result[type],
                                },
                            })
                    blocks.append({
                        'type': 'divider'
                    })

        slack_client.api_call(
            "chat.postMessage",
            channel=channel,
            blocks=blocks,
        )

def parse_trailer(
    title_json
):
    trailer = None
    if 'videos' in title_json:
        for result in title_json['videos']['results']:
            if result['type'] == "Trailer":
                trailer = result;

    return trailer

def convert_trailer_link(
    trailer_json
):
    if not trailer_json:
        return None

    if trailer_json['site'] == "YouTube":
        return "https://www.youtube.com/watch?v={}".format(
            trailer_json['key']
        )
    return None

def parse_title(
    title_id,
    channel,
    trailer_mode=False
):
    #
    # If this is a themoviedb format, then chances are we cached it
    # from a previous request.
    #
    moviedb_matches = re.search(MOVIEDB_REGEX, str(title_id))
    moviedb_info_json = None

    if moviedb_matches:
        moviedb_info_json = moviedb_client.get_cached(int(title_id))
        if not moviedb_info_json:
            slack_client.api_call(
                "chat.postMessage",
                channel=channel,
                text="Unknown title ID {}".format(title_id)
            )
            return
        #
        #
        #
        title_name = u""
        if 'title' in moviedb_info_json:
            title_name = moviedb_info_json['title']
        elif 'name' in moviedb_info_json:
            title_name = moviedb_info_json['name']
        title_msg = u"`{}` *{}*".format(
            title_id,
            title_name
        )

        #
        # Package it as an array
        #
        type = "tv_results" if moviedb_info_json['media_type'] == 'tv' else 'movie_results'
        moviedb_info_json = {
            type: [moviedb_info_json]
        }

        cast_msg = u"No cast information"

    else:
        r = title_get(title_id)

        if not r:
            slack_client.api_call(
                "chat.postMessage",
                channel=channel,
                text="Unknown title ID {}".format(title_id)
            )
            return

        json_response = r

        title_msg = u"`{}` *{}*".format(
            json_response['titleID'],
            json_response['title']
        )
        if 'startYear' in json_response:
            if (
                'endYear' in json_response and
                json_response['endYear'] != json_response['startYear']
            ):
                title_msg += u" ({}-{})".format(
                    json_response['startYear'],
                    json_response['endYear'],
                )
            else:
                title_msg += u" ({})".format(
                    json_response['startYear'],
                )

        cast_msg = u"No cast information"
        if ('cast' in json_response and len(json_response['cast']) > 0):
            cast_msg = u""
            for x in json_response['cast']:
                cast_msg += u"`{}` {}".format(
                    x['nameID'],
                    x['name']
                )
                if 'role' in x:
                    cast_msg += u" ({})".format(
                        u", ".join(x['role'])
                    )
                cast_msg += u"\n"

        #
        # Get extra title info from themoviedb
        #
#        moviedb_info_json = moviedb_client.get_title_by_imdb(title_id)
        moviedb_info_json = moviedb_client.get_title_by_imdb(title_id, get_trailer=True)

    msg_text = ""
    thumbnail = None
    attachments = []
    trailer = None
    moviedb_id = None
    media_type = None

    if moviedb_info_json:
#        moviedb_json = moviedb_info.json()
        for result in ['tv_results', 'movie_results']:
            if result in moviedb_info_json:
                for title in moviedb_info_json[result]:
                    if 'poster_path' in title:
                        #    "poster_sizes": [
                        #      "w92",
                        #      "w154",
                        #      "w185",
                        #      "w342",
                        #      "w500",
                        #      "w780",
                        #      "original"
                        #    ],
                        #            videos
                        #            tv_episode_results
                        #            tv_season_results
                        #            tv_results
                        #            person_results
                        #            movie_results
                        #
                        thumbnail = "{}{}".format(
                            "https://image.tmdb.org/t/p/w342",
                            title['poster_path']
                        )
                    if 'overview' in title:
                        msg_text = title['overview']
                    if 'id' in title:
                        moviedb_id = title['id']
                    if result == 'movie_results':
                        media_type = "movie"
                    elif result == 'tv_results':
                        media_type = "tv"

    msg_text += u"\n{}".format(cast_msg)

    if trailer_mode and moviedb_id:
        moviedb_info_json = moviedb_client.get_detail(
            moviedb_id,
            media_type
        )
        trailer = parse_trailer(moviedb_info_json)

        trailer_link = convert_trailer_link(trailer)
        if trailer_link:
            slack_client.api_call(
                "chat.postMessage",
                channel=channel,
                text=trailer_link
            )
            return
        else:
            attachments = [{
                'text': "I can't find a trailer"
            }]
    else:
        attachments = [{
            'pretext': title_msg,
            'thumb_url': thumbnail,
            'text': msg_text,
            'mrkdwn_in': ["text", "pretext"],
        }]
        ###
#        msg_section = {
#            'type': 'section',
#            'text': {
#                'text': msg_text,
#                'type': 'mrkdwn',
#            },
#        }
#        if thumbnail:
#            msg_section = {
#                'type': 'section',
#                'text': {
#                    'text': msg_text,
#                    'type': 'mrkdwn',
#                },
#                'accessory': {
#                    'image_url': thumbnail,
#                    'type': 'image',
#                    'alt_text': 'thumbnail',
#                },
#            }
#        blocks =[
#            {
#                'type': 'section',
#                'text': {
#                    'text': title_msg,
#                    'type': 'mrkdwn',
#                },
#            },
#            msg_section
#        ]
#        slack_client.api_call(
#            "chat.postMessage",
#            channel=channel,
#            blocks=blocks,
#        )
#        return
        ###

    slack_client.api_call(
        "chat.postMessage",
        channel=channel,
        attachments=attachments,
#        text=msg_text
    )

    return

def parse_bacon_solution(json_response):
    bacon_msg = None
    if len(json_response) > 0:
        bacon_msg = u""
        for x in json_response:
            if 'role' in x['from']:
                from_msg = u"{} ({})".format(
                    x['from']['name'],
                    u", ".join(x['from']['role'])
                )
            else:
                from_msg = x['from']['name']

            if 'role' in x['to']:
                to_msg = u"{} ({})".format(
                    x['to']['name'],
                    u", ".join(x['to']['role'])
                )
            else:
                to_msg = x['to']['name']

            if 'startYear' in x['title']:
                title_msg = u"{} ({})".format(
                    x['title']['title'],
                    x['title']['startYear'],
                )
            else:
                title_msg = u"{}".format(
                    x['title']['title'],
                )

            bacon_msg += u"{} was in *{}* with {}\n".format(
                from_msg,
                title_msg,
                to_msg,
            )
    return bacon_msg

def handle_command(
    command,
    channel,
    user,
    bacon_session
):
    command_args = command.lower().split(" ")
    op = command_args[0]

    print "op = {}".format(op)

    if op == "help":
        bacon_msg = (
            u"`bacon [title (titleID|title_name)|name (nameID|actor_name)]`\n"
            ">Plays Six Degrees of Kevin Bacon.  _title_ starts the path with a "
            "given movie/TV title, while _name_ finds the shortest path from "
            "an actor to Kevin Bacon. Specifying no parameters selects a random title as a starting point.\n"
        )
        bacon_msg += (
            u"`title (titleID|title_name)`\n"
            ">Fetches information for the given title.\n"
        )
        bacon_msg += (
            u"`name (nameID|actor_name)`\n"
            ">Fetches actor information for the given name.  Past roles are listed and capped at {}\n".format(
                max_titles_to_display
            )
        )
        bacon_msg += (
            u"`trailer (titleID|title_name)`\n"
            ">Fetches a movie trailer for the given title\n"
        )
#        bacon_msg += (
#            u"`![parameter]`\n"
#            ">Repeats a user's previous command with the given parameter.\n\n"
#        )
        bacon_msg += (
            u"I use :eyes: to indicate that I've seen your request, and :heavy_check_mark: to indicate that I've responded to it."
        )
        slack_client.api_call(
            "chat.postMessage",
            channel=channel,
            text=bacon_msg
        )
        return
    elif op == "name":
        if len(command_args) > 1:
            command_string = "name"
            offset = len(command_string)
            user_last_command_history[user] = command_string
            new_command = command[offset:].lstrip().rstrip()

            name_id = None
            matches = re.search(NAME_REGEX, new_command)
            if matches:
                name_id = new_command
            else:
                new_command = new_command.replace("&amp;", "&")
                r = name_search(new_command)

                if len(r.json()) > 1:
                    num_results, bacon_msg = disambiguate_names(r.json())
                    bacon_msg = u"Which one?\n>>>{}".format(bacon_msg)
                elif len(r.json()) == 0:
                    bacon_msg = u"No results for _{}_".format(new_command)
                else:
                    name_id = r.json()[0]['nameID']

            if name_id:
                parse_name (name_id, channel)
            else:
                slack_client.api_call(
                    "chat.postMessage",
                    channel=channel,
                    text=bacon_msg
                )
        else:
            slack_client.api_call(
                "chat.postMessage",
                channel=channel,
                text=u"Invalid command."
            )

        return
    elif op == "bacon":
        if len(command_args) == 1:
            user_last_command_history[user] = op
            bacon_url = "{}/bacon/random".format(rest_url)
            print "Sending request {}".format(bacon_url)
            r = bacon_session.get(bacon_url, timeout=rest_reply_timeout_secs)

            if r.status_code != requests.codes.ok:
                print (
                    "Unexpected response from REST server:\n{}".format(
                        r
                    )
                )
                r.raise_for_status()

            #
            # Format the JSON into a slack message
            #
            if len(r.json()) > 0:
                bacon_msg = parse_bacon_solution(r.json())
            else:
                bacon_msg = u"No solution found"

            slack_client.api_call(
                "chat.postMessage",
                channel=channel,
                text=bacon_msg
            )
            return
        elif len(command_args) > 1:
            if command_args[1] not in {'name', 'title'}:
                slack_client.api_call(
                    "chat.postMessage",
                    channel=channel,
                    text="You need to specify one of name or title"
                )
                return
            elif command_args[1] == "name":
                command_string = "bacon name"
                user_last_command_history[user] = command_string

                offset = len(command_string)
                params = command[offset:].rstrip().lstrip()

                matches = re.search(NAME_REGEX, params)
                name_id = None
                bacon_msg = None
                if matches:
                    name_id = params
                elif len(params) < 1:
                    bacon_msg = u"Invalid name"
                else:
                    resp = name_search(params)

                    num_results = len(resp.json())

                    if num_results == 1:
                        name_id = resp.json()[0]['nameID']
                    elif num_results > 1:
                        num_results, bacon_msg = disambiguate_names(resp.json())
                        bacon_msg = u"Which one?  Specify the exact one using the `nn1234567` identifier.\n>>>{}".format(bacon_msg)
                    else:
                        bacon_msg = u"No results for _{}_".format(
                            params
                        )

                if name_id:
                    resp = bacon_name(name_id)
                    if len(resp.json()):
                        bacon_msg = parse_bacon_solution(resp.json())
                    else:
                        bacon_msg = u"Couldn't find anything for _{}_".format(
                            name_id
                        )

                if bacon_msg:
                    slack_client.api_call(
                        "chat.postMessage",
                        channel=channel,
                        text=bacon_msg
                    )
                    return

            elif command_args[1] == "title":
                command_string = "bacon title"
                user_last_command_history[user] = command_string
                offset = len(command_string)
                params = command[offset:].rstrip().lstrip()

                matches = re.search(TITLE_REGEX, params)
                title_id = None
                bacon_msg = None
                if matches:
                    title_id = params
                elif len(params) < 1:
                    bacon_msg = u"Invalid title"
                else:
                    json_resp = title_search(params)

                    num_results = len(json_resp)

                    if num_results == 1:
                        title_id = json_resp[0]['titleID']
                    elif num_results > 1:
                        num_results, bacon_msg = disambiguate_titles(json_resp)
                        bacon_msg = u"Which one?  Specify the exact one using the `tt1234567` identifier.\n>>>{}".format(bacon_msg)
                    else:
                        bacon_msg = u"No results for _{}_".format(
                            params
                        )

                if title_id:
                    resp = bacon_title(title_id)
                    if len(resp.json()):
                        bacon_msg = parse_bacon_solution(resp.json())
                    else:
                        bacon_msg = u"Couldn't find anything for _{}_".format(
                            title_id
                        )

                if bacon_msg:
                    slack_client.api_call(
                        "chat.postMessage",
                        channel=channel,
                        text=bacon_msg
                    )
                    return


    elif op == "title" or op == "trailer":
        if len(command_args) > 1:
#            command_string = "title"
            command_string = op
            offset = len(command_string)
            user_last_command_history[user] = command_string
            new_command = command[offset:].lstrip().rstrip()
            trailer_mode = op == "trailer"

            title_id = None
            matches = re.search(TITLE_REGEX, new_command)
            moviedb_matches = re.search(MOVIEDB_REGEX, new_command)
            if matches or moviedb_matches:
                title_id = new_command
            else:
                new_command = new_command.replace("&amp;", "&")
                json_resp = title_search(new_command)

                if len(json_resp) > 1:
                    num_results, bacon_msg = disambiguate_titles(json_resp)
                    bacon_msg = u"Which one?\n>>>{}".format(bacon_msg)
                elif len(json_resp) == 0:
                    bacon_msg = u"No results for _{}_".format(new_command)
                else:
                    title_id = json_resp[0]['titleID']

            if title_id:
                parse_title (
                    title_id,
                    channel,
                    trailer_mode=trailer_mode
                )
            else:
                slack_client.api_call(
                    "chat.postMessage",
                    channel=channel,
                    text=bacon_msg
                )
        else:
            slack_client.api_call(
                "chat.postMessage",
                channel=channel,
                text=u"Invalid command."
            )
    elif op == "trending":
        handle_trending(channel)

def idle_work():
    time.sleep(1)

if __name__ == "__main__":
    if slack_client.rtm_connect(
        with_team_state=False,
        auto_reconnect=True
    ):
        print("Starter Bot connected and running!")
        # Read bot's user ID by calling Web API method `auth.test`
        starterbot_id = slack_client.api_call("auth.test")["user_id"]
        print "starterbot_id = {}".format(starterbot_id)

        #
        # Re-use the same session for each request
        #
        bacon_session = requests.Session()
        num_exceptions = 0
        last_exception_reset_time = None
        report_exception = True

        while True:
            msg_event = slack_client.rtm_read()
            for event in msg_event:
                command, channel, user, ts = parse_bot_commands(event)
                if command:
                    current_time = time.time()

                    #
                    # Resume reporting of exceptions to a Slack channel
                    # after a 5 minute period of silence.
                    #
                    if (
                        last_exception_reset_time and
                        current_time - last_exception_reset_time > exception_silence_interval_secs
                    ):
                        last_exception_reset_time = None
                        report_exception = True
                        print "Resuming exception reporting to slack"

                    try:
                        slack_client.api_call(
                            "reactions.add",
                            channel=channel,
                            name="eyes",
                            timestamp=ts
                        )

                        handle_command(command, channel, user, bacon_session)

                        slack_client.api_call(
                            "reactions.add",
                            channel=channel,
                            name="heavy_check_mark",
                            timestamp=ts
                        )
#                    except ConnectionError as nce:
                    except Exception as e:
                        # HTTPConnectionPool(host='10.1.2.52', port=9999): Max retries exceeded with url: /bacon/random (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7f364e29d150>: Failed to establish a new connection: [Errno 110] Connection timed out',)
                        slack_client.api_call(
                            "reactions.add",
                            channel=channel,
                            name="cry",
                            timestamp=ts
                        )

                        if report_exception:
                            #
                            # Strip out the api_key
                            #
                            exception_msg = re.sub(
                                "api_key=[a-zA-Z0-9]+",
                                "api_key=xxx",
                                str(e)
                            )

                            slack_client.api_call(
                                "chat.postMessage",
                                channel=channel,
                                text=u"Sorry, I encountered an error :(\n>```{}```".format(
                                    exception_msg
                                )
                            )

                        if not last_exception_reset_time:
                            last_exception_reset_time = current_time
                            num_exceptions = 1
                        else:
                            num_exceptions += 1
                            elapsed_time = current_time - last_exception_reset_time

                            #
                            # The exception timer resets every 5 minutes.  If we've
                            # hit 5 exceptions in that time frame, stop reporting it to
                            # a Slack channel.
                            #
                            if num_exceptions > max_exceptions_to_report_per_interval:
                                print "Stopping exception reporting to Slack"
                                report_exception = False

                        print "Exception: {}".format(e)
                        continue

            idle_work()

    else:
        print("Connection to Slack failed.")
