# moviebot

## Starting up the bot
1. Get a Slack token to use the API
1. Get a MovieDB token to use their API.  Go to https://www.themoviedb.org/ to get one.
1. Download this repo
1. `cd moviebot`
1. `virtualenv moviebot`
1. `source moviebot/bin/activate`
1. `pip install -r requirements.txt`
1. `MOVIEBOT_OAUTH_TOKEN=your_slack_token MOVIEBOT_MOVIEDB_TOKEN=your_moviedb_token python moviebot.py`
